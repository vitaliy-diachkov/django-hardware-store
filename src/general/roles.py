from rolepermissions.roles import AbstractUserRole


class Cashier(AbstractUserRole):
    """Cashier user role

    A cashier is a person who gets the orders from customers and
    redirects them to shop assitants. After receiving all needed
    products from assistants they counts all shopping list
    and gives customer a receipt (so that they "finalize"
    orders, changing their status to PAID).

    Attributes:
        name (str): A role string representation
        avaliable_permissions (Dict[str, bool]): Role permissions
    """

    name = 'cashier'
    available_permissions = {
        'can_create_order': True,
        'can_read_completed_order': True,
        'can_create_receipt': True,
    }


class ShopAssistant(AbstractUserRole):
    """Shop assistant user roles

    Shop assistant is a person, who gets customer's order from
    cashiear and collects all needed products from stop's
    storage, giving it all back to customer and chaning
    the order status to COMPLETED.

    Attributes:
        name (str): A role string representation
        avaliable_permissions (Dict[str, bool]): Role permissions
    """

    name = 'shop_assistant'
    available_permissions = {
        'can_read_new_order': True,
        'can_complete_order': True
    }


class Accountant(AbstractUserRole):
    """Accountant user role

    Accountant is a person who account all the shop profit. He can
    access to all of the customers' orders information.

    Attributes:
        name (str): A role string representation
        avaliable_permissions (Dict[str, bool]): Role permissions
    """

    name = 'accountant'
    available_permissions = {
        'can_manage_orders': True
    }
