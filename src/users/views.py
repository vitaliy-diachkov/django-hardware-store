from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.contrib.auth import login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View
from django.views.generic.edit import FormView, CreateView

from users.forms import UserCreateForm, UserLoginForm


class UserCreateView(CreateView):
    """
    Registration (sign up) view
    """

    form_class = UserCreateForm
    template_name = 'users/register.html'
    success_url = reverse_lazy('users:login')


class UserLoginView(FormView):
    """
    A simple login View
    """
    template_name = 'users/login.html'
    form_class = UserLoginForm
    success_url = reverse_lazy('general:index')

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return super().form_valid(form)


class UserLogoutView(LoginRequiredMixin, View):
    """
    A simple logout view
    """
    def get(self, request):
        logout(request)
        return HttpResponseRedirect(reverse_lazy('general:index'))
