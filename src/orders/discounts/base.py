class BaseAbstractDiscount:
    """Abstract product discount

    Inherit this class to make yout own discount.
    """

    NO_DISCOUNT = 0

    def get_discount_amount(self, order):
        """
        Override this method to add extra login for discount accrual.

        Args:
            order (Order): A order which products should be discounted

        Returns:
            Decimal: Discount amount in currency
        """

        return self.NO_DISCOUNT
