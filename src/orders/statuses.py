from django.db import models


class OrderStatus(models.IntegerChoices):
    NEW = 1
    COMPLETED = 2
    PAID = 3
