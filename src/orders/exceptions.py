class InvalidOrderStatusException(Exception):
    pass


class DiscounterConfigError(Exception):
    pass
