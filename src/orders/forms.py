from django import forms

from sentry_sdk import capture_message

from products.models import Product

from orders.models import Order, Receipt
from orders.statuses import OrderStatus
from orders.services import validate_order_id


class OrderForm(forms.ModelForm):
    """
    An order creation form
    """

    product = forms.ModelChoiceField(
        queryset=Product.objects.all(),
        empty_label='Select product'
    )

    class Meta:
        model = Order
        fields = ('product',)

    def save(self):
        order = super().save()

        # sentry logging
        capture_message(
            f'A new order with ID {order.id} was created'
            f'(product: {order.product.name})',
            level='info'
        )

        return order


class OrderCompleteForm(forms.Form):
    """
    Gets the new order by its ID and resets its status to COMPLETED
    """

    id = forms.IntegerField()

    def clean_id(self):
        order_id = self.cleaned_data.get('id')
        return validate_order_id(order_id)

    def clean(self):
        cleaned_data = super().clean()

        order = Order.objects.get(id=cleaned_data['id'])

        if order.status != OrderStatus.NEW:
            raise forms.ValidationError(
                'Invalid order status: only new orders can become completed',
                code='invalid_order_status'
            )

        return cleaned_data

    def save(self):
        order = Order.objects.get(id=self.cleaned_data['id'])
        order.complete()

        capture_message(
            f'Order ID {order.id} has chenged status to COMPLETED',
            level='info'
        )

        return order


class ReceiptForm(forms.Form):
    """
    Gets the completed order by ID and resets its status to PAID. Creates
    receipt, based on the given order.
    """

    id = forms.IntegerField()

    def clean_id(self):
        order_id = self.cleaned_data.get('id')
        return validate_order_id(order_id)

    def clean(self):
        cleaned_data = super().clean()
        order = Order.objects.get(id=cleaned_data['id'])

        if order.status != OrderStatus.COMPLETED:
            raise forms.ValidationError(
                f'Only completed order can be finilized',
                code='invalid_order_status'
            )

        return cleaned_data

    def save(self):
        order = Order.objects.get(id=self.cleaned_data['id'])
        return Receipt.objects.create_receipt(
            order=order
        )
