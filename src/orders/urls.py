from django.urls import path

from orders.views import (
    OrderCreateView,
    OrderListView,
    NewOrderListView,
    CompletedOrderListView,
    OrderCompleteView,
    ReceiptCreateView
)


app_name = 'orders'

urlpatterns = [
    path('create/', OrderCreateView.as_view(), name='create'),
    path('', OrderListView.as_view(), name='list'),
    path('new/', NewOrderListView.as_view(), name='new'),
    path('completed/', CompletedOrderListView.as_view(), name='completed'),
    path('finilize/', ReceiptCreateView.as_view(), name='finilize'),
    path('complete/', OrderCompleteView.as_view(), name='complete'),
]
